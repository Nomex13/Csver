﻿using System;
using System.Linq;

namespace Iodynis.Libraries.Csving.Example
{
    public static class Program
    {
        public static void Main(params string[] arguments)
        {
            Csv csv = new Csv();

            csv.Read("../../Example.csv");

            // Make some changes
            csv["ColumnOne", "RowOne"] = "=^_^=";
            csv["ColumnNew", "RowNew"] = "ValueNew";

            Console.WriteLine($"Columns: {String.Join(", ", csv.Columns)}.");
            Console.WriteLine($"Rows: {String.Join(", ", csv.Rows)}.");
            Console.WriteLine();

            // Print what it looks like in the file
            Console.WriteLine();
            Console.WriteLine(csv.Print());

            // Print it pretty
            int width = 12;
            string delimiterVertical = " | ";
            string delimiterHorizontal = new String('-', width);
            // Header
            Console.WriteLine($"{csv.KeyColumnName.PadRight(width)}{delimiterVertical}{String.Join(delimiterVertical, csv.Columns.Select(_value => _value.PadRight(width)))}");
            // Delimiter
            Console.WriteLine($"{delimiterHorizontal} | {String.Join(delimiterVertical, csv.Columns.Select(_value => delimiterHorizontal))}");
            // Rows
            foreach (string row in csv.Rows)
            {
                Console.Write($"{row.PadRight(width)}");
                foreach (string column in csv.Columns)
                {
                    Console.Write($"{delimiterVertical}");
                    Console.Write($"{(csv[column, row] ?? "null").PadRight(width)}");
                }
                Console.WriteLine();
            }

            csv.Write("../../ExampleModified.csv");

            Console.ReadKey();
        }
    }
}
